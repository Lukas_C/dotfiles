{
  description = "Lukas_C - System and home configuration";

  nixConfig = { extra-sandbox-paths = "/var/tmp/agenix-rekey"; };

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # Core framework for this flake.
    hive = {
      url = "github:divnix/hive";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    std.follows = "hive/std";
    haumea.follows = "hive/std/haumea";
    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixos-generators = {
      url = "github:nix-community/nixos-generators";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # Secrets management.
    ragenix = {
      url = "github:yaxitech/ragenix";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        rust-overlay.follows = "rust-overlay";
      };
    };
    agenix-rekey = {
      url = "github:oddlama/agenix-rekey";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    # Private flake containing secrets as NixOS modules.
    # This can be overridden when deploying using
    # `--override-input secrets <flake URI>`.
    # See the custom version of `nh` in this flake for handling this automatically.
    secrets.url = "github:divnix/blank";

    # Additional dependencies
    # TODO: remove once xmonad-contrib-0.19 releases with the required patch of `dynamicSBs`.
    # Relevant PR:
    # https://github.com/xmonad/xmonad-contrib/pull/878
    xmonad = {
      url = "github:xmonad/xmonad";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.unstable.follows = "nixpkgs";
    };
    xmonad-contrib = {
      url = "github:xmonad/xmonad-contrib";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.xmonad.follows = "xmonad";
    };
    barstate-hs = {
      url = "gitlab:Lukas_C/barstate-hs";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    crablock = {
      url = "gitlab:Lukas_C/crablock";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        rust-overlay.follows = "rust-overlay";
      };
    };
  };

  outputs = inputs@{ self, hive, std, agenix-rekey, nixpkgs, secrets, ... }:
    hive.growOn {
      inherit inputs;

      cellsFrom = ./nix;
      cellBlocks = let
        inherit (std.blockTypes) functions devshells installables pkgs;
        inherit (hive.blockTypes) diskoConfigurations nixosConfigurations;
      in [
        # For more information on profiles, suites and modules, see the `github:divnix/hive` flake, in the file `./src/collect.nix`.
        # Profiles: modular chunks of (system) configuration.
        (functions "hardwareProfiles")
        (functions "nixosProfiles")
        (functions "userProfiles")
        (functions "homeProfiles")

        # Suites: bundle sets of profiles.
        (functions "nixosSuites")
        (functions "homeSuites")

        # Modules: custom modules for entirely new functionality.
        (functions "nixosModules")
        (functions "homeModules")

        # Configurations: (system) outputs of the flake.
        # Usage: `disko [options] --flake <flake URI>#<disk-config>`
        # From the disko CLI:
        # > With flakes, `disk-config` is discovered first under the `.diskoConfigurations` top level attribute
        # > or else from the disko module of a NixOS configuration of that name under `.nixosConfigurations`.
        # In this case, the `diskoConfigurations` are not exported, meaning that disko will only use `nixosConfigurations`.
        diskoConfigurations
        nixosConfigurations

        # Package sets.
        (pkgs "pkgs")
        # Secrets.
        (functions "secrets")

        # Flake utility.
        (functions "lib")
        (devshells "devshells")
        (installables "installables")
      ];
    }
    # Soil: export configurations from collected attributes above.
    {
      nixosConfigurations = let
        # Ignore cell names when aggregating configurations, using the block names directly.
        # SAFETY: Identical configuration names from different cells might overwrite each other.
        # However, this repo should never contain two configurations with identical hostnames.
        collectDiscardCellName = hive.collect // {
          renamer = cell: target: "${target}";
        };
      in collectDiscardCellName self "nixosConfigurations";

      # Compatibility outputs for the `agenix` CLI.
      agenix-rekey = agenix-rekey.configure {
        userFlake = self;
        nodes = self.nixosConfigurations;
      };

      # Devshells for `nix develop` and use with `direnv`.
      devShells = std.harvest self [ "flake" "devshells" ];

      # General purpose helper functions, for potential reuse elsewhere.
      lib = std.harvest self [ "flake" "lib" ];
    };
}

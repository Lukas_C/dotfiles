{ inputs, cell }:

let
  inherit (inputs) nixpkgs;
  inherit (nixpkgs) lib;
in import nixpkgs {
  inherit (nixpkgs) system;
  config = {
    allowUnfreePredicate = pkg:
      builtins.elem (lib.getName pkg) [
        # Unknown license.
        "aspell-dict-en-science"

        # Nvidia proprietary drivers.
        "nvidia-x11"
        "nvidia-settings"

        # Blackmagic capture card drivers.
        "blackmagic-desktop-video"
        "decklink"

        "discord"
      ];
    permittedInsecurePackages = [
      # Logseq still requires an old electron version.
      # See `./homeProfiles/logseq.nix` for details.
      "electron-27.3.11"
    ];
  };
}

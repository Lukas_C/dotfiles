{ inputs, cell }:

let
  inherit (cell) nixosProfiles userProfiles;

  base = [
    nixosProfiles.base
    nixosProfiles.secrets
    nixosProfiles.systemdBoot
    nixosProfiles.mntFolder
  ];

  # Intended for systems with a GUI.
  pc = base ++ [
    nixosProfiles.cups
    nixosProfiles.fwupd
    nixosProfiles.gtk
    nixosProfiles.libinput
    nixosProfiles.locale
    nixosProfiles.openssh
    nixosProfiles.pipewire
    nixosProfiles.pki
    nixosProfiles.secretsManagement
    nixosProfiles.smartd
    nixosProfiles.tuigreet
    nixosProfiles.xsession

    nixosProfiles.homeManager
    userProfiles.finn

    nixosProfiles.podman
  ];

  laptop = [
    nixosProfiles.light

  ];

  crossCompilation = [ nixosProfiles.binfmt ];
in { inherit base pc laptop crossCompilation; }

{ inputs, cell }:

{
  mkWireguardInterface = config:
    # `config.baseConfig` and `config.peerConfig` usually come from a secrets flake.
    config.baseConfig // {
      autostart = false;
      peers = [ config.peerConfig ];
    } // builtins.removeAttrs config [ "baseConfig" "peerConfig" ];
}

{
  device = "/dev/vda";
  type = "disk";

  content = {
    type = "gpt";

    partitions = {
      ESP = {
        size = "512M";
        type = "EF00";

        content = {
          type = "filesystem";
          format = "vfat";
          mountpoint = "/boot";
          mountOptions = [ "defaults" ];
        };
      };
      root = {
        name = "root";
        size = "100%";

        content = {
          type = "btrfs";
          extraArgs = [ "-f" ];
          subvolumes = {
            "/root" = {
              mountpoint = "/";
              mountOptions = [ "compress=zstd" "noatime" ];
            };
            "/home" = {
              mountpoint = "/home";
              mountOptions = [ "compress=zstd" "noatime" ];
            };
            "/nix" = {
              mountpoint = "/nix";
              mountOptions = [ "compress=zstd" "noatime" ];
            };
            "/swap" = {
              mountpoint = "/.swapvol";
              swap.swapfile.size = "8G";
            };
          };
        };
      };
    };
  };
}

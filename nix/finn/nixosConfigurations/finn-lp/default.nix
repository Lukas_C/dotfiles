{ inputs, cell, config }:

let
  inherit (inputs) home-manager;
  inherit (cell) secrets;
  inherit (cell.lib) mkWireguardInterface;
  hostName = "finn-lp";
  stateVersion = "23.11";
in {
  bee = {
    system = "x86_64-linux";
    pkgs = cell.pkgs;
    home = home-manager;
  };
  networking = { inherit hostName; };
  system = { inherit stateVersion; };
  age.rekey.hostPubkey = builtins.readFile ./_ssh_host_ed25519_key.pub;

  imports = let inherit (cell) hardwareProfiles nixosProfiles nixosSuites;
  in builtins.concatLists [
    [ hardwareProfiles."${hostName}" ]

    nixosSuites.pc
    nixosSuites.laptop
    nixosSuites.crossCompilation # For remote deploys.

    [
      (secrets [ "nixosModules" "wireguard-finn-lp" ] { })
      (secrets [ "nixosModules" "wireguard-work" ] { })
    ]
  ];

  services.xserver = {
    enable = true;
    # TODO: complete migration to greetd
    # displayManager.gdm.enable = true;
    displayManager.startx.enable = true;
    desktopManager.gnome.enable = true;

    xkb = {
      # Comma syntax for multiple layouts is a feature of xkb config:
      # https://unix.stackexchange.com/a/197639
      layout = "de,us";
      variant = "nodeadkeys,altgr-intl";
      model = "pc105";
      # Caps lock works as an additional Super key.
      # Switch layouts by pressing both Alt keys. AltGr functionality is retained.
      # Layout status is indicated by the Caps Lock LED.
      # See xkeyboard-config(7) for more options.
      options = "caps:super,grp:alt_altgr_toggle,grp_led:caps";
    };
  };

  console.useXkbConfig = true;

  networking.networkmanager.enable = true;
  networking.wg-quick.interfaces = let
    mkSrv = peerConfigName:
      mkWireguardInterface {
        address = [ "10.13.13.111" ];
        baseConfig = (secrets [ "wireguard" "configs" "srv" ] { });
        privateKeyFile = config.age.secrets."wireguard-finn-lp".path;
        peerConfig = (secrets [ "wireguard" "peers" peerConfigName ] { }) // {
          presharedKeyFile = config.age.secrets."wireguard-finn-lp-psk".path;
        };
      };
  in {
    "srv" = mkSrv "srv";
    "srv-full" = mkSrv "srvFull";

    "work" = {
      autostart = false;
      configFile = config.age.secrets."wireguard-work".path;
    };
  };

  hardware.enableRedistributableFirmware = true;

  hardware.printers = {
    # TODO: https://github.com/NixOS/nixpkgs/issues/78535#issuecomment-2200268221
    ensurePrinters = [{
      name = "HP_LaserJet_P2035";
      location = "Home";
      deviceUri = "ipps://print.lan/printers/HP_LaserJet_P2035_IPP";
      model = "everywhere";
      ppdOptions = { PageSize = "A4"; };
    }];
    ensureDefaultPrinter = "HP_LaserJet_P2035";
  };

  users.mutableUsers = false;
  home-manager.users."finn" = {
    home = { inherit stateVersion; };

    imports = let inherit (cell) homeProfiles homeSuites;
    in builtins.concatLists [
      homeSuites.base
      homeSuites.backups
      homeSuites.tools
      homeSuites.xmonad
      homeSuites.gui
      homeSuites.passwords
      homeSuites.dev

      [
        (secrets [ "homeModules" "emailPersonal" ] { })
        (secrets [ "homeModules" "emailWork" ] { })
      ]
    ];

    programs.autorandr.enable = true;
  };
}

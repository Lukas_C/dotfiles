{ inputs, cell }:

let
  inherit (inputs) home-manager nixos-generators;
  hostName = "finn-bootstrap";
  stateVersion = "23.11";
in {
  bee = {
    system = "x86_64-linux";
    pkgs = cell.pkgs;
    home = home-manager;
  };
  networking = { inherit hostName; };
  system = { inherit stateVersion; };

  imports =
    let inherit (cell) hardwareProfiles nixosProfiles nixosSuites userProfiles;
    in builtins.concatLists [
      [ nixos-generators.nixosModules.all-formats ]

      nixosSuites.base
      [ nixosProfiles.secretsManagement ]
    ];

  users.users."root".openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIP4pkElcMtGlbU3uhholjjJuL31SBCGChnAXmAG+O07d"
  ];

  # Add a faster build option for prototyping, see:
  # https://nixos.wiki/wiki/Creating_a_NixOS_live_CD#Building_faster
  # TODO: move to "flake" cell and make available as a flake output.
  formatConfigs."install-iso-fast" = {
    imports = [ nixos-generators.nixosModules.install-iso ];
    isoImage.squashfsCompression = "gzip -Xcompression-level 1";
  };
}

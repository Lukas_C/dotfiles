{ inputs, cell, lib, config }:

let
  inherit (inputs) home-manager;
  inherit (cell) secrets;
  inherit (cell.lib) mkWireguardInterface;
  hostName = "finn-pc";
  stateVersion = "23.11";
in {
  bee = {
    system = "x86_64-linux";
    pkgs = cell.pkgs;
    home = home-manager;
  };
  networking = { inherit hostName; };
  system = { inherit stateVersion; };
  age.rekey.hostPubkey = builtins.readFile ./_ssh_host_ed25519_key.pub;

  imports = let inherit (cell) hardwareProfiles nixosProfiles nixosSuites;
  in (builtins.concatLists [
    [ hardwareProfiles."${hostName}" ]

    nixosSuites.pc
    nixosSuites.crossCompilation # For remote deploys.

    [ (secrets [ "nixosModules" "wireguard-finn-pc" ] { }) ]
  ]);

  boot.loader.systemd-boot.windows."Windows" = {
    title = "Windows";
    efiDeviceHandle = "HD1e65535a1";
  };

  services.xserver = {
    enable = true;
    xkb = {
      layout = "us";
      variant = "altgr-intl";
      model = "pc104";
    };
  };

  console.useXkbConfig = true;

  systemd.network.enable = true;
  networking.useDHCP = lib.mkForce false;
  systemd.network.networks."10-lan" = {
    matchConfig.Name = "enp34s0";
    networkConfig.DHCP = "yes";
  };
  networking.wg-quick.interfaces = let
    mkSrv = peerConfigName:
      mkWireguardInterface {
        address = [ "10.13.13.110" ];
        baseConfig = (secrets [ "wireguard" "configs" "srv" ] { });
        privateKeyFile = config.age.secrets."wireguard-finn-pc".path;
        peerConfig = (secrets [ "wireguard" "peers" peerConfigName ] { }) // {
          presharedKeyFile = config.age.secrets."wireguard-finn-pc-psk".path;
        };
      };
  in {
    "srv" = mkSrv "srv";
    "srv-full" = mkSrv "srvFull";
  };

  hardware.enableRedistributableFirmware = true;

  users.mutableUsers = false;
  home-manager.users."finn" = {
    home = { inherit stateVersion; };

    imports = let inherit (cell) homeProfiles homeSuites;
    in builtins.concatLists [
      homeSuites.base
      homeSuites.backups
      homeSuites.tools
      homeSuites.xmonad
      homeSuites.gui
      homeSuites.passwords
      homeSuites.dev
      homeSuites.chat
      homeSuites.capture

      [ (secrets [ "homeModules" "emailPersonal" ] { }) ]
    ];
  };
}

{ inputs, cell, config, pkgs }:

let inherit (cell) secrets;
in {
  imports = [ (secrets [ "nixosModules" "finn" ] { }) ];

  programs.zsh.enable = true;

  users.users."finn" = {
    isNormalUser = true;
    createHome = true;
    extraGroups = [
      "wheel"
      # Access to backlight control
      "video"
    ];
    shell = pkgs.zsh;
    hashedPasswordFile = config.age.secrets."finn".path;

    openssh.authorizedKeys.keys = let inherit (inputs) nixosConfigurations;
    in [
      # TODO
      # nixosConfigurations.finn-lp.config.age.rekey.hostPubkey

      # finn-pc (Arch Linux)
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIP4pkElcMtGlbU3uhholjjJuL31SBCGChnAXmAG+O07d"
    ];
  };
}

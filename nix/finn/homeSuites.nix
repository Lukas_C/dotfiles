{ inputs, cell }:

let
  inherit (cell.pkgs) lib;
  inherit (cell) homeProfiles;

  base = [
    homeProfiles.xdgDirs
    homeProfiles.xdgMimeApps
    homeProfiles.shellAliases

  ];

  # Creating and maintaining backups.
  backups = [ homeProfiles.borgmatic ];

  # Terminal and general tools.
  tools = [
    # Handling the system config (i.e. this flake).
    homeProfiles.config

    # Shell
    homeProfiles.zsh
    homeProfiles.starship

    # Editor
    homeProfiles.emacs

    # File and Repository Management
    homeProfiles.croc
    homeProfiles.direnv
    homeProfiles.git

    # System utilities
    homeProfiles.eza
    homeProfiles.bat
    homeProfiles.fd
    homeProfiles.fzf
    homeProfiles.dust

    # Misc
    homeProfiles.rage
    homeProfiles.remoteAccess
  ];

  # xmonad-based DE configuration.
  xmonad = [
    homeProfiles.xmonad
    homeProfiles.xmobar
    homeProfiles.rofi
    homeProfiles.dunst

    homeProfiles.picom
    homeProfiles.gammastep

    homeProfiles.crablock
  ];

  # Graphical applications.
  gui = [
    # Core
    homeProfiles.theme
    homeProfiles.xdgPortal
    homeProfiles.darkman

    # Navigation
    homeProfiles.kitty
    homeProfiles.thunar

    # Visual utilities
    homeProfiles.qalculate
    homeProfiles.meld
    homeProfiles.remmina

    # Web applications
    homeProfiles.firefox
    homeProfiles.thunderbird

    # Documents
    homeProfiles.okular
    homeProfiles.logseq
    homeProfiles.libreoffice
    homeProfiles.rnote
    homeProfiles.xournalpp
    homeProfiles.sxiv
    homeProfiles.gimp
    homeProfiles.inkscape
  ];

  # Password management
  passwords = [ homeProfiles.keepass ];

  # Support for ad-hoc development without having to instanciate a flake first.
  dev = [ homeProfiles.texlive ];

  # Personal chat applications.
  chat = [
    homeProfiles.discord
    homeProfiles.element

  ];

  # Utilities for video capture and streaming.
  capture = [ homeProfiles.obs ];

  # Everything
  full = lib.attrValues homeProfiles;
in { inherit base backups tools xmonad gui passwords dev chat capture full; }

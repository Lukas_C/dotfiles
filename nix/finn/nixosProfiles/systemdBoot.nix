{ inputs, cell }:

{
  boot.loader.efi.canTouchEfiVariables = true;

  boot.loader.systemd-boot = {
    enable = true;

    editor = false;
    consoleMode = "auto";
  };
}

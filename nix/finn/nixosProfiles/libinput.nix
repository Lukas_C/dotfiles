{ inputs, cell }:

{
  services.libinput = {
    enable = true;

    mouse = {
      # TODO: Custom accel curve:
      # https://wayland.freedesktop.org/libinput/doc/latest/pointer-acceleration.html
      accelProfile = "flat";
    };

    touchpad.naturalScrolling = true;
  };
}

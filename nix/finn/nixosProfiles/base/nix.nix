{ inputs, cell, lib }:

let
  inputsNoSelf = lib.filterAttrs (n: _:
    !(builtins.elem n [
      "self" # Handled separately
      "cells" # Artifact of divnix/hive and not an actual flake
      "secrets" # Keep secrets out of the registry
    ])) inputs;
  registryInputs = lib.mapAttrs (_: v: { flake = v; }) inputsNoSelf;
  nixPathInputs = lib.mapAttrsToList (n: v: "${n}=${v}") inputsNoSelf;
in {
  extraOptions = "experimental-features = nix-command flakes";

  # Pin the system registry and `$NIX_PATH` to the flake inputs.
  registry = registryInputs // {
    dotfiles.flake = { inherit (inputs.self) outPath; };
  };
  nixPath = nixPathInputs;

  gc = {
    automatic = true;
    dates = "weekly";
    randomizedDelaySec = "45min";
    options = "--delete-older-than 90d";
  };
  optimise = {
    automatic = true;
    dates = [ "weekly" ];
  };
}

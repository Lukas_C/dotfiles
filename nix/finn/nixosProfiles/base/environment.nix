{ inputs, cell, pkgs }:

{
  systemPackages = with pkgs; [
    # system utils
    binutils
    coreutils

    inetutils
    ldns

    usbutils
    pciutils

    dosfstools
    btrfs-progs

    # files
    bat
    fd
    file
    ripgrep
    tree

    # processes
    htop
    killall

    # archives
    zip
    unzip
    gnutar

    # misc
    git
    neovim
  ];
}

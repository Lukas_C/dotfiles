{ inputs, cell }:

{
  services.printing = {
    enable = true;
    startWhenNeeded = true;
    stateless = true;

    webInterface = true;
    cups-pdf.enable = true;
  };
}

{ inputs, cell }:

let
  inherit (inputs) ragenix agenix-rekey;
  agenixRekeyCache = "/var/tmp/agenix-rekey";
in {
  imports = [
    ragenix.nixosModules.default
    agenix-rekey.nixosModules.default

  ];

  # agenix-rekey options
  age.rekey = {
    masterIdentities = [
      ./_identities/finn-yubikey-c.pub
      ./_identities/finn-yubikey-a.pub

    ];

    # Store rekeyed secrets out-of-band.
    storageMode = "derivation";
  };

  # Alternate cache directory, see:
  # https://github.com/oddlama/agenix-rekey/issues/9
  nix.settings.extra-sandbox-paths = [ agenixRekeyCache ];
  age.rekey.cacheDir = ''${agenixRekeyCache}/"$UID"'';
  systemd.tmpfiles.rules = [ "d ${agenixRekeyCache} 1777 root root" ];
}

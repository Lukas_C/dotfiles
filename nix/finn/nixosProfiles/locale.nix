{ inputs, cell }:

{
  i18n = {
    defaultLocale = "en_US.UTF-8";

    extraLocaleSettings = {
      LC_ADDRESS = "de_DE.UTF-8";
      LC_COLLATE = "de_DE.UTF-8";
      #LC_CTYPE = "";
      #LC_IDENTIFICATION = "";
      LC_MONETARY = "de_DE.UTF-8";
      #LC_MESSAGES = "";
      LC_MEASUREMENT = "de_DE.UTF-8";
      #LC_NAME = "";
      #LC_NUMERIC = "";
      LC_PAPER = "de_DE.UTF-8";
      LC_TELEPHONE = "de_DE.UTF-8";

      # de_DE would result in German Weekdays/Months,
      # en_US would result in wrong MM/DD/YY date formatting.
      LC_TIME = "en_GB.UTF-8";
    };

    supportedLocales = [
      "en_US.UTF-8/UTF-8"
      "de_DE.UTF-8/UTF-8"
      "en_GB.UTF-8/UTF-8"
      "C.UTF-8/UTF-8"
    ];
  };

  time.timeZone = "Europe/Berlin";
}

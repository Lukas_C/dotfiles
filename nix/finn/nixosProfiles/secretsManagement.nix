{ inputs, cell, pkgs }:

{
  # Yubikey support
  services.pcscd.enable = true;
  # Required in order for regular users to be able to talk to the pcscd daemon.
  security.polkit.enable = true;

  # PIN prompt
  environment.systemPackages = with pkgs; [ pinentry-curses ];
}

{ inputs, cell }:

{
  # Dconf is basically a hard dependency for most GTK 3/4 applications.
  programs.dconf.enable = true;
}
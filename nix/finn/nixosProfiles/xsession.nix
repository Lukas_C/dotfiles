{ inputs, cell, pkgs }:

{
  # `startx` needs to be enabled manually.
  # Only adding the `xorg.xinit` package will not configure any additional modules:
  # https://www.reddit.com/r/NixOS/comments/kr0j9v/im_very_confused_by_how_nixos_configures_xorg/
  services.xserver.displayManager.startx.enable = true;

  services.xserver.displayManager.session = [
    # Dummy session for running an .xsession.
    # The situation around that is a bit messy at the moment:
    # https://github.com/NixOS/nixpkgs/issues/177555
    # https://github.com/NixOS/nixpkgs/issues/190442
    # Related issues with gnome-keyring not working:
    # https://github.com/NixOS/nixpkgs/issues/174099#issuecomment-1201697954
    # https://github.com/nix-community/home-manager/issues/1454
    # Using the workaround proposed here:
    # https://github.com/NixOS/nixpkgs/issues/177555#issuecomment-1895240117
    {
      name = "xsession";
      manage = "desktop";
      start = ''
        ${pkgs.runtimeShell} $HOME/.xsession &
        waitPID=$!
      '';
    }
  ];
}

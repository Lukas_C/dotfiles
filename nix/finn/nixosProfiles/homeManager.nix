{ inputs, cell }:

{
  imports = [ inputs.home-manager.nixosModules.home-manager ];

  home-manager = {
    # Share packages between system and users
    useGlobalPkgs = true;
    useUserPackages = true;
  };

  # Using the option `useUserPackages` apparently leads to a lot less linkage:
  # https://github.com/nix-community/home-manager/pull/5158#issuecomment-2020125193
  environment.pathsToLink =
    [ "/share/applications" "/share/xdg-desktop-portal" ];
}

{ inputs, cell }:

{
  # https://nixos.wiki/wiki/NixOS_on_ARM#Compiling_through_binfmt_QEMU
  boot.binfmt.emulatedSystems = [ "aarch64-linux" ];
}

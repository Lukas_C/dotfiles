{ inputs, cell, pkgs }:

{
  services.greetd = {
    enable = true;
    # Launch on tty2 to prevent issues with boot log overdraw.
    vt = 2;
    settings = {
      default_session = {
        command =
          "${pkgs.greetd.tuigreet}/bin/tuigreet --time --remember --remember-user-session";
        user = "greeter";
      };
    };
  };
}

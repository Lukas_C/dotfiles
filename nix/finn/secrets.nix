{ inputs, cell }:

let
  inherit (inputs) secrets;
  inherit (inputs.nixpkgs) lib;
in {
  # Use a functor to comply with paisano requirement that a block needs to be an attribute set.
  # NOTE: UX would greatly benefit from a "proxy" that lazily
  # computes the attribute set, similar to the proposal here:
  # https://github.com/NixOS/nix/issues/8187
  __functor = _:
    # Attribute path to look up.
    path:
    # Default value in case the value is not found.
    # We cannot provide a default here, since we
    # don't know what would have been stored there.
    fallback:
    lib.attrByPath path (builtins.warn
      ''secret "${builtins.concatStringsSep "." path}" is not available''
      fallback) secrets;
}

{ inputs, cell, lib }:

{
  imports = [ ./_generated.nix ];

  # Nvidia graphics and display configuration.
  hardware.graphics.enable = true;
  hardware.nvidia = {
    open = false;
    nvidiaSettings = true;
  };
  # A lot of information about the nvidia configuration options are included in the driver releases:
  # https://download.nvidia.com/XFree86/Linux-x86_64/
  services.xserver = {
    videoDrivers = [ "nvidia" ];
    # The nvidia driver tries to extract the DPI from the EDID information, which is wrong.
    dpi = 96;
    # TODO: Increase refresh rate of secondary monitors.
    # TODO: Does not work correctly with nvidia drivers.
    # Deployed config is instead generated manually with `nvidia-settings`
    # and placed at `/etc/X11/xorg.conf.d/95-nvidia.conf`.
    xrandrHeads = [
      {
        output = "DP-0";
        monitorConfig = ''
          VertRefresh 50.0 - 75.0
          Option "DPMS"

          # Option "Position" "0 0"
          Option "Rotate" "left"
        '';
      }
      {
        output = "DP-2";
        primary = true;
        monitorConfig = ''
          HorizSync 222.0 - 222.0
          VertRefresh 40.0 - 144.0
          Option "DPMS"

          # Option "Position" "1080 240"
          Option "Rotate" "normal"
        '';
      }
      {
        output = "DP-4";
        monitorConfig = ''
          VertRefresh 50.0 - 75.0
          Option "DPMS"

          # Option "Position" "3640 0"
          Option "Rotate" "right"
        '';
      }
    ];
  };

  boot.kernelParams = [
    # Disables ttys on secondary monitors.
    "video=DP-1:d"
    "video=DP-3:d"
  ];

  # Enable support for Blackmagic capture cards.
  hardware.decklink.enable = true;
}

{ inputs, cell, config }:

{
  services.dunst = {
    enable = true;

    # TODO: Icons do not apply correctly.
    iconTheme = config.theming.gtk.icons;

    settings = let inherit (config.theming) colors;
    in {
      global = {
        monitor = 0;
        follow = 0;

        width = 300;
        origin = "top-right";
        offset = "20x52";

        progress_bar = true;
        progress_bar_height = 10;
        progress_bar_frame_width = 0;

        transparency = 20;

        separator_height = 1;
        frame_width = 1;

        idle_threshold = "1m";

        font = "${config.theming.font.name} 10";
        enable_recursive_icon_lookup = true;

        background = colors.bg1;
        foreground = colors.fg;
        frame_color = colors.bg1;
        timeout = 10;
      };

      urgency_critical = {
        frame_color = colors.red;
        timeout = 0;
      };
    };
  };
}

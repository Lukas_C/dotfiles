{ inputs, cell, config, pkgs }:

{
  home.packages = with pkgs; [
    rage
    age-plugin-yubikey
    # PIN prompt
    pinentry-curses
  ];
}

{ inputs, cell, config }:

{
  services.darkman = {
    enable = true;
    # Disable automatic theme switching.
    settings.usegeoclue = false;
  };

  xdg.portal = {
    extraPortals = [ config.services.darkman.package ];
    config.common = { "org.freedesktop.impl.portal.Settings" = "darkman"; };
  };
}

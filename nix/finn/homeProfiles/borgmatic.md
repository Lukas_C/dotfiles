# First Time Setup
When installing for the first time on a new system, run:
``` sh
borgmatic init --encryption keyfile --append-only
```

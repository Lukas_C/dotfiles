{ inputs, cell, lib, pkgs }:

let inherit (cell) secrets;
in {
  home.packages = let
    # Helper script that sends a magic wakeup packet to a host collected from `secrets.devices`.
    # Requires that both `devices.<name>.ipAddress` and `devices.<name>.macAddress` are set.
    wol-host = let
      hostsVariables = with lib;
        pipe (secrets [ "devices" ] { }) [
          # Check that the necessary keys are present
          (filterAttrs (_: v: v ? ipAddress && v ? macAddress))
          (attrsToList)
          (map (v: ''
            ips["${v.name}"]="${v.value.ipAddress}"
            macs["${v.name}"]="${v.value.macAddress}"''))
          (concatStringsSep "\n")
        ];
    in pkgs.writeShellScriptBin "wol-host" ''
      declare -A ips
      declare -A macs
      ${hostsVariables}

      while [[ $# -gt 0 ]]; do
        case $1 in
          -l|--list)
            echo "Available hosts:"
            for dev in ''${!ips[@]}; do
              echo "$dev"
              echo "  ''${ips[$dev]}"
              echo "  ''${macs[$dev]}"
            done
            ;;
          *)
            if [[ -v ips[$1] ]]; then
              ${pkgs.wol}/bin/wol -i "''${ips[$1]}" "''${macs[$1]}"
            else
              echo "unknown host: $1"
            fi
            ;;
        esac
        shift
      done
    '';
  in with pkgs; [
    # SSH
    openssh
    # SSH via UDP
    mosh
    # Send Wake on LAN packets to devices.
    wol
    wol-host
    # File transfer.
    rsync
  ];
}

{ inputs, cell }:

{
  services.picom = {
    enable = true;

    # For a description of available window types, see:
    # https://specifications.freedesktop.org/wm-spec/latest/ar01s05.html
    # wintypes = { };

    # Set backend to modern default.
    backend = "glx";
    vSync = true;

    fadeExclude = [
      # https://github.com/google/xsecurelock/issues/97#issuecomment-1183086902
      "class_g = 'xsecurelock'"
      "class_g = 'Crablock' && class_i = 'xsecurelock'"

      "class_g = 'Rofi' && class_i = 'rofi'"
    ];

    settings = {
      shadow = true;
      # Shadows centered behind window
      shadow-radius = 7;
      shadow-offset-x = -7;
      shadow-offset-y = -7;

      fade = true;
      no-fading-openclose = true;

      active-opacity = 1.0;
      inactive-opacity = 0.95;
      frame-opacity = 0.7;
      focus-exclude = [ "class_g = 'VirtualBox Machine'" ];
      opacity-rule = [ "100:name = 'Picture-in-Picture'" ];

      detect-rounded-corners = true;
      use-ewmh-active-win = true;
      use-damage = true;
    };
  };
}

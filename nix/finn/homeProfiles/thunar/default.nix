{ inputs, cell, lib, pkgs }:

let thunarPackage = pkgs.xfce.thunar;
in {
  # TODO: Create dedicated module.

  home.packages = [ thunarPackage ] ++ (with pkgs.xfce; [
    # Required for settings to be saved, see:
    # https://github.com/NixOS/nixpkgs/issues/65771
    # Apparently having thunar running in daemon mode with
    # ```
    # Type = "dbus"
    # ```
    # seems to suffice for everything to be loaded correctly.
    xfconf

    # NOTE: depends on gvfs in system space
    thunar-volman

    thunar-archive-plugin
    thunar-media-tags-plugin
  ]);

  xfconf = {
    enable = true;
    # List available settings with
    # ```
    # xfconf-query -c thunar --list
    # ```
    settings.thunar = {
      "last-view" = "ThunarDetailsView";

    };
  };

  # Assemble "User Custom Actions" XML file "by hand".
  # TODO: UX would benefit if it were possible to create the XML structure from nix code.
  # Again, this config in general should likely be done as a dedicated module.
  # TODO: figure out how to deal with `<unique-id></unique-id>`.
  home.file.".config/Thunar/uca.xml".text = let
    actions = with lib;
      let dir = ./_actions;
      in pipe dir [
        builtins.readDir
        # Filter anything that is not a file.
        (filterAttrs (_: ftype: ftype == "regular"))
        (attrNames)
        # Convert file names to a full path, read and join.
        (map (fname: dir + ("/" + fname)))
        (map builtins.readFile)
        (concatStringsSep "\n")
      ];
  in ''
    <?xml version="1.0" encoding="UTF-8"?>
    <actions>
    ${actions}
    </actions>
  '';

  systemd.user.services."thunar" = {
    Unit = {
      Description = "Thunar file manager";
      Documentation = [ "man:Thunar(1)" ];
    };

    Service = {
      Type = "dbus";
      BusName = "org.xfce.FileManager";
      ExecStart = "${thunarPackage}/bin/Thunar --daemon";
      Restart = "on-failure";
      KillMode = "process";
    };

    Install.WantedBy = [ "graphical-session.target" ];
  };
}

{ inputs, cell, pkgs, config }:

{
  home.packages = with pkgs; [ sxiv ];

  xresources.properties = let inherit (config.theming) colors;
  in {
    "Sxiv.foreground" = colors.fg;
    "Sxiv.background" = colors.bg;
  };

  xdg.mimeApps = let
    defaults = {
      "image/png" = "sxiv.desktop";
      "image/jpeg" = "sxiv.desktop";
      "image/tiff" = "sxiv.desktop";
    };
  in {
    associations.added = defaults;
    defaultApplications = defaults;
  };
}

{ inputs, cell, pkgs }:

{
  home.packages = with pkgs; [ libsForQt5.okular ];

  xdg.mimeApps = let
    defaults = {
      "application/pdf" = "okularApplication_pdf.desktop";

    };
  in {
    associations.added = defaults;
    defaultApplications = defaults;
  };
}

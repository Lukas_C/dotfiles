{ inputs, cell, config }:

{
  programs.bat = {
    enable = true;

    config = { theme = with config.theming.theme; "${primary}-${mode}"; };
  };

  # Use bat as manpager.
  programs.zsh.initExtra = ''
    export MANPAGER="sh -c 'col -bx | bat -l man -p'"
    export MANROFFOPT="-c"
  '';
}

{ inputs, cell }:

{
  programs.eza.enable = true;

  home.shellAliases = {
    "ls" = "eza";
    "la" = "eza --group-directories-first -la";
    "ll" = "eza --group-directories-first -l";
    "lt" = "eza --tree";
  };
}

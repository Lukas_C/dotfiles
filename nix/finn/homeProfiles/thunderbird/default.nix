{ inputs, cell }:

{
  programs.thunderbird = {
    enable = true;

    profiles."default" = {
      isDefault = true;
      userChrome =
        builtins.readFile ./_userChrome/top_element_bold_if_unread.css;
    };
  };
}

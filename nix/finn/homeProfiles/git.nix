{ inputs, cell, config }:

{
  programs.git = {
    enable = true;
    lfs.enable = true;
    difftastic.enable = true;

    aliases = {
      # Fancy commit graph in the terminal.
      "graph" = "log --all --decorate --oneline --graph";
      # A safer "push --force", avoids overwriting other peoples work.
      "pushf" = "push --force-if-includes";
    };

    extraConfig = {
      init.defaultBranch = "main";

      diff.tool = "meld";
      merge.tool = "meld";

      # TODO: use `includeIf` directive to configure `user.name` and `user.email` conditionally:
      # https://git-scm.com/docs/git-config#_conditional_includes
    };
  };

  # Git specific shell aliases.
  home.shellAliases = {
    "ga" = "git add";
    "gs" = "git status";
    "gd" = "git diff";
    "gdc" = "git diff --cached";
    "gc" = "git commit";
    "gl" = "git log";
    "gg" = "git graph";

    "grs" = "git restore --staged";
    "gpsh" = "git push";
    "gpll" = "git pull";

    # Change to the root of a repository.
    "cdg" = ''cd "$(git rev-parse --show-toplevel)"'';
  };
}

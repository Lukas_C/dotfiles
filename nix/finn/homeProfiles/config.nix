{ inputs, cell, config }:

let
  inherit (inputs) cells std;
  inherit (cells.flake) installables;
  defaultFlakePath = "${config.home.homeDirectory}/dotfiles";
in {
  home.packages = [
    std.packages.default

    installables.nh-secrets
  ];

  home.sessionVariables = {
    "FLAKE_DOTFILES" = "${defaultFlakePath}";
    # Optional, currently not required, as "nh-wrapped"
    # looks for secrets relative to the `FLAKE_DOTFILES` environment variable.
    # "FLAKE_SECRETS" = "${defaultFlakePath}/secrets";
  };
}

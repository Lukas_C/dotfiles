{ inputs, cell, pkgs }:

{
  home.packages = with pkgs; [ gimp ];

  xdg.mimeApps = {
    associations.added = {
      "image/gif" = "gimp.desktop";
      "image/jpeg" = "gimp.desktop";
      "image/png" = "gimp.desktop";
      "image/tiff" = "gimp.desktop";
    };
  };
}

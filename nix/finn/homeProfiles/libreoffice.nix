{ inputs, cell, pkgs }:

{
  home.packages = with pkgs; [ libreoffice ];

  xdg.mimeApps = let
    # MS document (and open document) mime types:
    # https://stackoverflow.com/a/4212908
    defaults = {
      "application/vnd.openxmlformats-officedocument.wordprocessingml.document" =
        "writer.desktop";
      "application/vnd.openxmlformats-officedocument.wordprocessingml.template" =
        "writer.desktop";
      "application/msword" = "writer.desktop";

      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" =
        "calc.desktop";
      "application/vnd.openxmlformats-officedocument.spreadsheetml.template" =
        "calc.desktop";
      "application/vnd.ms-excel" = "calc.desktop";

      "application/vnd.openxmlformats-officedocument.presentationml.presentation" =
        "impress.desktop";
      "application/vnd.openxmlformats-officedocument.presentationml.template" =
        "impress.desktop";
      "application/vnd.openxmlformats-officedocument.presentationml.slideshow" =
        "impress.desktop";
      "application/vnd.ms-powerpoint" = "impress.desktop";
    };
  in {
    associations.added = defaults;
    defaultApplications = defaults;
  };
}

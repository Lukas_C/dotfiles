{ inputs, cell, config, lib, pkgs }:

{
  # NOTE: Getting portals to work with home-managers `useUserPackages` requires
  # additional options in the systems `environment.pathsToLink`:
  # https://github.com/nix-community/home-manager/pull/5158#issuecomment-2020125193
  xdg.portal = {
    enable = true;
    xdgOpenUsePortal = true;

    extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
    config.common = { "default" = "gtk"; };
  };

  # Required for xdg-desktop-portals to be able to locate default applications:
  # https://github.com/NixOS/nixpkgs/issues/189851
  xsession.importedVariables = [ "PATH" ];
}

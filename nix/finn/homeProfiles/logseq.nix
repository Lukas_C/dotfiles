{ inputs, cell, pkgs }:

{
  home.packages = with pkgs;
    [
      # Logseq requires old versions of electron:
      # https://github.com/NixOS/nixpkgs/issues/341683
      # Source:
      # https://github.com/NixOS/nixpkgs/blob/master/pkgs/by-name/lo/logseq/package.nix
      logseq
    ];
}

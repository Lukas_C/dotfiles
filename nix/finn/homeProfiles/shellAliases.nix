{ inputs, cell, config, lib }:

{
  home.shellAliases = {
    # Magic snippet allowing for aliases to still work in a sudo environment.
    # POSIX spec, 2.3.1 "Alias Substitution":
    # https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_03_01
    # > If the value of the alias replacing the word ends in a <blank>,
    # > the shell shall check the next command word for alias substitution;
    "sudo" = "sudo ";
    # sudoedit
    "se" = "sudo -e";

    # `ls` aliases, but overridable, so that they can be replaced by something like `eza`.
    "la" = lib.mkDefault "ls -lhBa";
    "ll" = lib.mkDefault "ls -lhB";

    # Generic open command.
    "o" = "xdg-open";

    # "Editorclient": open an instance of the default editor, daemonized if possible.
    "ec" = if config.services.emacs.defaultEditor then
      "emacsclient -nc"
    else
      "$EDITOR";

    # Misc program shorthands.
    "py" = "python";
    "odump" = "objdump -dC -Mintel";
  };
}

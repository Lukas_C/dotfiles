{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
------------------------------------------------------------------------
-- Imports
------------------------------------------------------------------------
import XMonad
import qualified XMonad.StackSet as W

import Control.Arrow (second)
import Control.Monad (liftM)
import Data.List (find, isSuffixOf)
import Data.Maybe (fromMaybe, isNothing)
import System.Exit (exitSuccess)

-- Actions
import XMonad.Actions.WithAll (sinkAll, killAll)
import XMonad.Actions.UpdatePointer (updatePointer)

-- Hooks
import XMonad.Hooks.EwmhDesktops (ewmh, ewmhFullscreen, setEwmhActivateHook)
import XMonad.Hooks.FadeWindows (FadeHook, fadeWindowsLogHook, fadeWindowsEventHook, opaque, isUnfocused, transparency)
import XMonad.Hooks.Focus (manageFocus, newOnCur, activateSwitchWs, liftQuery)
import XMonad.Hooks.ManageDocks (ToggleStruts(..))
import XMonad.Hooks.SetWMName (setWMName)
import XMonad.Hooks.StatusBar (dynamicEasySBs)
import XMonad.Hooks.StatusBar.Barstate.DBus (statusBarDBus)
import XMonad.Hooks.UrgencyHook (doAskUrgent)

-- Layout
import XMonad.Layout.LayoutModifier (ModifiedLayout(..))
import XMonad.Layout.MultiToggle (EOT(EOT), (??), Toggle(..), mkToggle)
import XMonad.Layout.MultiToggle.Instances (StdTransformers(FULL, NOBORDERS))
import XMonad.Layout.Renamed (Rename(Replace), renamed)
import XMonad.Layout.Spacing (Spacing, Border(..), spacingRaw)

-- Util
import XMonad.Util.EZConfig (additionalKeysP)
import XMonad.Util.NamedScratchpad (NamedScratchpad(NS), customFloating, namedScratchpadAction, namedScratchpadManageHook)
import XMonad.Util.SpawnOnce (spawnOnce, spawnOnOnce)



------------------------------------------------------------------------
-- General Settings
------------------------------------------------------------------------
-- Default Programs
myBrowser       = "firefox"
myTerminal      = "kitty"
myMusicClient   = "vimpc"
myVolumeGui     = "pavucontrol"


-- Quick Visual Settings
myBorderWidth = 1                 -- Width of the window border in pixels
mySpacingSize = 5                 -- Spacing around windows in pixels

myNormalBorderColor  = "#1d2021"  -- Gray
myFocusedBorderColor = "#458588"  -- Blue

myFadeInactiveAmount = 0.05       -- Fade amount for inactive windows


-- Quick Key Settings
myModMask = mod4Mask



------------------------------------------------------------------------
-- Key Bindings
------------------------------------------------------------------------
myKeys :: [(String, X ())]
myKeys =
  -- Window Controls
    [ ("M-j",                   windows W.focusDown)   -- Move focus down
    , ("M-k",                   windows W.focusUp)     -- Move focus up
    , ("M-m",                   windows W.focusMaster) -- Focus Master

    , ("M-S-j",                 windows W.swapDown)    -- Swap down
    , ("M-S-k",                 windows W.swapUp)      -- Swap up
    , ("M-S-m",                 windows W.swapMaster)  -- Swap with Master

    , ("M-h",                   sendMessage Shrink)    -- Shrink Master
    , ("M-l",                   sendMessage Expand)    -- Expand Master

    , ("M-n",                   refresh)               -- Refresh windows

    , ("M-S-c",                 kill)                  -- Close current window
    , ("M-S-M1-c",              killAll)               -- Close all windows on the current workspace

  -- Layout Controls
    , ("M-<Tab>",               sendMessage NextLayout)            -- Cycle through layouts

    , ("M-.",                   sendMessage $ IncMasterN 1)        -- Increase windows in the master area
    , ("M-,",                   sendMessage $ IncMasterN (-1))     -- Decrease windows in the master area
    
    , ("M-t",                   withFocused $ windows . W.sink)    -- Push focused window back to tiling
    , ("M-M1-t",                sinkAll)                           -- Push all windows back to tiling

    , ("M-<Space>",             sendMessage (Toggle FULL) >> sendMessage ToggleStruts)    -- Toggle fullscreen

  -- Launching Applications
    , ("M-<Return>",            spawn myTerminal)

    , ("M-S-<Return>",          spawn "rofi -show run")

    , ("M-S-M1-<Return> b",     spawn myBrowser)
    , ("M-S-M1-<Return> c",     spawn "rofi -show calc")
    , ("M-S-M1-<Return> e",     spawn "emacsclient -c")
    , ("M-S-M1-<Return> x",     spawn "xournalpp")

  -- Scratchpads
    , ("M-0",                   namedScratchpadAction myNamedScratchpads myVolumeGui)
    , ("M-S-0",                 namedScratchpadAction myNamedScratchpads myMusicClient)

  -- Media Keys
    , ("<XF86AudioPlay>",       spawn "mpc toggle")
    , ("<XF86AudioNext>",       spawn "mpc next")
    , ("<XF86AudioPrev>",       spawn "mpc prev")
    , ("<XF86AudioRaiseVolume>",spawn $ "wpctl set-volume '@DEFAULT_AUDIO_SINK@' '5%+';" ++ volumeNotifyCommand)
    , ("<XF86AudioLowerVolume>",spawn $ "wpctl set-volume '@DEFAULT_AUDIO_SINK@' '5%-';" ++ volumeNotifyCommand)
    , ("<XF86AudioMute>",       spawn $ "wpctl set-mute '@DEFAULT_AUDIO_SINK@' toggle;" ++ volumeNotifyCommand)

  -- Special Keys
    , ("<XF86MonBrightnessUp>",  spawn "light -A 10")
    , ("<XF86MonBrightnessDown>",spawn "light -U 10")
    , ("<Print>",               spawn "flameshot screen -c")
    , ("S-<Print>",             spawn "flameshot gui")

  -- XMonad
    , ("M-M1-l",                spawn "auth_crablock")
    , ("M-q",                   spawn "xmonad --restart")   -- Restart without recompiling, since the latter is managed by Home Manager.
    , ("M-S-q",                 io exitSuccess)             -- Quit Xmonad

  -- NOTE: "M-S-v" is being used for KeePassXC autotype
    ]
  -- Remap the monitor focus binds
    ++
    [ (mask ++ "M-" ++ [key], screenWorkspace scr >>= flip whenJust (windows . action))
        | (key, scr)  <- zip "wer" [1,0,2]
        , (action, mask) <- [ (W.view, "")
                            , (W.shift, "S-")
                            ]
    ]
  -- Add options for greedy and non-greedy workspace switch
    ++
    [ (otherModMasks ++ "M-" ++ [key], action tag)
        | (tag, key)  <- zip myWorkspaces "123456789"
        , (otherModMasks, action) <- [ ("", windows . W.greedyView)
                                     , ("S-", windows . W.shift)
                                     , ("M1-", windows. W.view)
                                     ]
    ]
    where
      volumeNotifyCommand = "dunstify -r 2593 \"$(wpctl get-volume '@DEFAULT_AUDIO_SINK@')\""



------------------------------------------------------------------------
-- Workspaces
------------------------------------------------------------------------
myWorkspaces :: [String]
myWorkspaces = ["main","browser","third","fourth","virt","chat","seventh","eighth","ninth"]



------------------------------------------------------------------------
-- Layouts
------------------------------------------------------------------------
-- Adds a one variable solution for setting the gaps of windows
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True


-- Create a LayoutClass that enables dynamic switching between two layouts, see:
-- https://www.reddit.com/r/xmonad/comments/fhzw3/permonitor_layout/
-- NOTE: The original version derives SLS only from "Show", which results in an error for me.
-- Here it is additionally derived from "Read".
data SLS sl ol a = SLS sl ol deriving (Read, Show)
instance (LayoutClass sl a, LayoutClass ol a, Typeable a) => LayoutClass (SLS (sl a) (ol a)) a
    where
      -- Glance at the current screen to determine which layout to run
      runLayout (W.Workspace i (SLS sl ol) ms) rect = do
        mts <- findScreenByTag i
        -- Uses the first layout if the screen is oriented horizontally otherwise uses the second layout
        case liftM ((\r -> fromEnum (rect_width r) > fromEnum (rect_height r)) . screenRect . W.screenDetail) mts of
          Just True -> fmap (second . fmap $ \nsl -> SLS nsl ol)
                       $ runLayout (W.Workspace i sl ms) rect
          _ -> fmap (second . fmap $ \nol -> SLS sl nol)
               $ runLayout (W.Workspace i ol ms) rect
        where
          findScreenByTag :: WorkspaceId -> X (Maybe (W.Screen WorkspaceId (Layout Window) Window ScreenId ScreenDetail))
          findScreenByTag i' = find ((== i') . (W.tag . W.workspace)) <$> gets (W.screens . windowset)


      -- Route messages to both sub-layouts
      handleMessage (SLS sl ol) m = do
        msl <- handleMessage sl m
        mol <- handleMessage ol m
        pure $ if isNothing msl && isNothing mol
          then Nothing
          else Just $ SLS (fromMaybe sl msl) (fromMaybe ol mol)

-- Helper to nail down the types of `SLS`
mkSLS :: (LayoutClass sl a, LayoutClass ol a) => sl a -> ol a -> SLS (sl a) (ol a) a
mkSLS = SLS


-- Default master/stacked layout
tall    = renamed [Replace "tall"]
        $ mySpacing mySpacingSize
        $ Tall 1 (3/100) (1/2)
-- Fully vertically stacked layout
stacked = renamed [Replace "stacked"]
        $ mySpacing mySpacingSize
        $ Tall 0 (3/100) (1/2)
-- Fullscreen layout respecting xmobar
full    = renamed [Replace "full"]
        $ Full

-- Tall layout that reconfigures to stacked only if not on the primary monitor (sid 0)
tallstacker = renamed [Replace "tall"]
            $ mkSLS tall stacked


myLayoutHook = mkToggle (NOBORDERS ?? FULL ?? EOT)    -- Add a toggle for true fullscreen
             $ myDefaultLayout
             where
               myDefaultLayout =   tallstacker
                               ||| full



------------------------------------------------------------------------
-- Window rules
------------------------------------------------------------------------
myManageHook :: ManageHook
myManageHook = composeAll
  -- Workspace assignment
    [ className =? "firefox"            --> doShift ( myWorkspaces !! 1 )
    , className =? "Vivaldi-stable"     --> doShift ( myWorkspaces !! 1 )
    , className =? "VirtualBox Manager" --> doShift ( myWorkspaces !! 4 )
    , className =? "discord"            --> doShift ( myWorkspaces !! 5 )
    , className =? "Element"            --> doShift ( myWorkspaces !! 5 )
    , className =? "Mumble"             --> doShift ( myWorkspaces !! 5 )
    , className =? "thunderbird"        --> doShift ( myWorkspaces !! 5 )
    , className =? "steam"              --> doShift ( myWorkspaces !! 6 )

  -- Window rules
    , className =? "VirtualBox Machine"             --> doFloat
    , className =? "Gimp"                           --> doFloat
    , className =? "matplotlib"                     --> doFloat
    , className =? "Onboard"                        --> doFloat     -- onscreen Keyboard

  -- Projucer
    , className =? "Projucer"
      <&&> title =? "Projucer"                      --> doFloat
    , className =? "Projucer"
      <&&> not <$> title =? "Projucer"              --> doIgnore    -- HACK: fixes popup windows
  -- Firefox
    , appName   =? "Places"                         --> doFloat     -- Library/Downloads
    , appName   =? "Toolkit"                        --> doFloat     -- PiP mode
  -- Thunar
    , className =? "Thunar"
      <&&> title =? "File Operation Progress"       --> doFloat
  -- KeePassXC
    , className =? "KeePassXC"
      <&&> title =? "Download Favicons"             --> doFloat

  -- Other
    , resource  =? "desktop_window"                 --> doIgnore
    , resource  =? "kdesktop"                       --> doIgnore 
    ]

myActivateHook :: ManageHook
myActivateHook = doAskUrgent  -- Mark windows as urgent instead of immediately focusing them
  -- If the window requesting focus is on the currently focused WS, activate it.
  -- This prevents some weirdness where the window will
  -- not get properly focused or hidden behind the main window.
    <> manageFocus (newOnCur --> liftQuery activateSwitchWs)

myFadeHook :: FadeHook
myFadeHook = composeAll
  -- Default fading behaviour
    [ opaque
    , isUnfocused                                   --> transparency myFadeInactiveAmount

  -- OBS
    , className =? "obs"
      <&&> ( title =? "Windowed Projector (Preview)"
        <||> title =? "Fullscreen Projector (Preview)"
           )                                        --> opaque
  -- Okular
    , className =? "okular"
      <&&> fmap (isSuffixOf " – Presentation — Okular") title
                                                    --> opaque

  -- Other
    , title =? "Picture-in-Picture"                 --> opaque
    ]



------------------------------------------------------------------------
-- Scratchpads
------------------------------------------------------------------------
myScratchpadManageHook :: ManageHook
myScratchpadManageHook = namedScratchpadManageHook myNamedScratchpads

myNamedScratchpads :: [NamedScratchpad]
myNamedScratchpads = 
  [ NS myMusicClient spawnMusic findMusic manageMusic     -- Musicplayer set in the general settings
  , NS myVolumeGui spawnVolume findVolume manageVolume    -- Volume GUI set in the general settings
  ]
  where
    sizeX = 0.4
    sizeY = 0.8
  -- Setup for music client scratchpad
    spawnMusic  = myTerminal ++ " --class " ++ myMusicClient ++ " '" ++ myMusicClient ++ "'" -- e.g. "kitty --class vimpd 'vimpd'"
    findMusic   = className =? myMusicClient
    manageMusic = customFloating $ W.RationalRect (0.25-sizeX/2.0) (0.5-sizeY/2.0) sizeX sizeY -- x y w h
  -- Setup for volume gui scratchpad
    spawnVolume  = myVolumeGui
    findVolume   = className =? myVolumeGui
    manageVolume = customFloating $ W.RationalRect (0.75-sizeX/2.0) (0.5-sizeY/2.0) sizeX sizeY -- x y w h

        

------------------------------------------------------------------------
-- Log Hook
------------------------------------------------------------------------
myLogHook :: X ()
myLogHook = do
  updatePointer (0.5, 0.5) (0.5, 0.5)   -- Pointer follows window focus



------------------------------------------------------------------------
-- Startup Hook
------------------------------------------------------------------------
myStartupHook :: X ()
myStartupHook = do
  spawnOnce "nitrogen --restore"        -- Restore wallpaper



------------------------------------------------------------------------
-- Main Loop
------------------------------------------------------------------------
main :: IO ()
main = xmonad
  $ setEwmhActivateHook myActivateHook . ewmhFullscreen . ewmh        -- Enable EWMH support
  $ dynamicEasySBs (statusBarDBus (\sid -> "xmobar -x" ++ show sid))  -- Automatic bar management
  $ def
  -- General stuff
    { terminal           = myTerminal

    , borderWidth        = myBorderWidth
    , normalBorderColor  = myNormalBorderColor
    , focusedBorderColor = myFocusedBorderColor

    , modMask            = myModMask

    , workspaces         = myWorkspaces

  -- Hooks, layouts
    , layoutHook         = myLayoutHook
    , manageHook         = myManageHook <> myScratchpadManageHook
    , logHook            = myLogHook <> fadeWindowsLogHook myFadeHook
    , startupHook        = myStartupHook <> setWMName "LG3D"
    , handleEventHook    = fadeWindowsEventHook

  -- Keybinds
    } `additionalKeysP` myKeys

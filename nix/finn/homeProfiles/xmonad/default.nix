{ inputs, cell, pkgs }:

let inherit (inputs) barstate-hs xmonad-contrib;
in {
  xsession = {
    enable = true;

    windowManager.xmonad = {
      enable = true;
      extraPackages = _: [
        # NOTE: barstate requires a change in the type signature of `dynamicSBs` that is not in xmonad-contrib-0.18.0.
        (barstate-hs.packages.default.override {
          xmonad-contrib = xmonad-contrib.defaultPackage;
        })
        xmonad-contrib.defaultPackage
      ];

      # TODO: centralize theming
      config = ./_xmonad.hs;
    };
  };

  home.packages = with pkgs; [
    # Wallpaper
    nitrogen
    # Audio volume GUI
    pavucontrol
    # Screen capture
    flameshot
  ];
}

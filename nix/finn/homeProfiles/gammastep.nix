{ inputs, cell }:

let inherit (cell) secrets;
in {
  services.gammastep = {
    enable = true;

    temperature = {
      day = 6500;
      night = 2500;
    };
    provider = "manual";

    latitude = secrets [ "location" "latitude" ] null;
    longitude = secrets [ "location" "longitude" ] null;
  };
}

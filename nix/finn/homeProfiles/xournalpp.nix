{ input, cell, pkgs }:

{
  home.packages = with pkgs; [ xournalpp ];

  xdg.mimeApps = let
    defaults = {
      # Provided by xournalpp package in ./share/mime/packages.
      # Gets installed to ~/.nix-profile/share/mime/packages.
      "application/xojpp" = "com.github.xournalpp.xournalpp.desktop";
      "application/x-xopp" = "com.github.xournalpp.xournalpp.desktop";
      "application/x-xopt" = "com.github.xournalpp.xournalpp.desktop";
    };
  in {
    associations.added = defaults // {
      "application/pdf" = "com.github.xournalpp.xournalpp.desktop";
    };
    defaultApplications = defaults;
  };
}

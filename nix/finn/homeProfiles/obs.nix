{ input, cell, pkgs }:

{
  home.packages = with pkgs;
    [
      # Obs needs to be compiled explicitly with decklink support:
      # https://obsproject.com/forum/threads/pcie-capture-card-not-detected-by-obs-anymore.160731/post-589264
      # https://github.com/NixOS/nixpkgs/blob/23e89b7da85c3640bbc2173fe04f4bd114342367/pkgs/applications/video/obs-studio/default.nix#L52
      (obs-studio.override { decklinkSupport = true; })
    ];
}

{ inputs, cell, pkgs }:

{
  home.packages = with pkgs; [ inkscape ];

  xdg.mimeApps = {
    associations.added = {
      "image/svg+xml" = "org.inkscape.Inkscape.desktop";
      "application/pdf" = "org.inkscape.Inkscape.desktop";
    };
  };
}

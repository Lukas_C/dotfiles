module Main where

import Xmobar

import Data.Barstate (FullStatus(..), WorkspaceStatus(..), ScreenStatus(..))
import Xmobar.Data.PP (StatePP)
import Xmobar.Plugins.DBusLog (configWithDBusLog)

import Control.Monad (filterM)
import Data.List (intercalate)
import System.Exit (die)
import System.Posix.Env (getEnv, getEnvDefault)
import System.Posix.Files (fileExist)

type Color = String

colorFg :: Color
colorFg = "#ebdbb2"

colorBg :: Color
colorBg = "#1d2021"

colorPrimary :: Color
colorPrimary = "#458588"

colorUrgent :: Color
colorUrgent = "#fb4933"

colorFocused :: Color
colorFocused = "#ebdbb2"

colorUnfocused :: Color
colorUnfocused = "#504945"

colorEmpty :: Color
colorEmpty = "#3c3836"

getDefaultConfig :: IO Config
getDefaultConfig = do
  -- get a sensible config path
  homeEnv <- getEnv "HOME"
  home <- case homeEnv of
    Just h -> pure h
    Nothing -> die "could not locate $HOME, please check your environment."
  configHomeEnv <- getEnvDefault "XDG_CONFIG_HOME" (home ++ "/.config")

  -- get status dirs of system batteries
  batteryDirs <- filterM (\dirname -> fileExist $ "/sys/class/power_supply/" ++ dirname) ["BAT0"]

  pure $ defaultConfig
    { font = "Iosevka SS06 Extended 11"
    , bgColor = colorBg
    , fgColor = colorFg
    , position = TopH 32
    , hideOnStart = False
    , persistent = True
    , iconRoot = configHomeEnv ++ "/xmobar"
    , commands =
      -- Time and date
      [ Run $ Date (concat [ "%H:%M"
                           , wrapColor colorPrimary " - "
                           , "%d %b %Y"
                           ]) "date" 50
      -- Cpu usage in percent
      , Run $ Cpu [ "-t"
                  , concat [ wrapColor colorPrimary "cpu: ("
                           , "<total>%"
                           , wrapColor colorPrimary ")"
                           ]
                  , "-H"
                  , "50"
                  , "--high"
                  , "#fb4933"
                  ] 20
      -- Ram used number and percent
      , Run $ Memory [ "-t"
                     , concat [ wrapColor colorPrimary "mem: <used>M ("
                              , "<usedratio>%"
                              , wrapColor colorPrimary ")"
                              ]
                     ] 20
      -- TODO: Check for updates?
      --, Run $ ComX (home ++ "/.local/bin/updatecheck") [] "" "updates" 3600
      -- Battery state
      , Run $ BatteryP batteryDirs [ "-t"
                                   , concat [ wrapColor colorPrimary "batt: <timeleft>h ("
                                            , "<left>%"
                                            , wrapColor colorPrimary ")"
                                            ]
                                   , "--High", "75"
                                   , "--Low", "20"
                                   , "--low", colorUrgent
                                   ] 50
      ]
    , sepChar = "%"
    , alignSep = "}{"
    , template = concat $ [ "} "
                          , "<icon=haskell_logo.xpm/>"
                          , separator
                          , "%DBusLog%"
                          , "{ "
                          --, wrapColor colorUrgent "%updates%"
                          ] ++
                 (if null batteryDirs then [] else
                     [ "%battery%"
                     , separator
                     ]) ++
                 [ "%cpu%"
                 , separator
                 , "%memory%"
                 , separator
                 , "%date%"
                 , " "
                 ]
    }

ppState :: StatePP FullStatus
ppState sid state =
  concat [ intercalate " " $ map ppWs $ fs_workspaces state
         , separator
         , sc_layout currentScreenState
         , separator
         , sc_windowTitle currentScreenState
         ]
  where
    currentScreenState = (fs_screens state) !! sid

    ppWs ws =
      wrapColor color
      $ uncurry wrapText chars
      $ ws_name ws
      where
        color | ws_isUrgent ws            = colorUrgent
              | ws_isCurrent ws
                && ws_visibleOn ws == sid = colorFocused
              | ws_visibleOn ws > -1      = colorUnfocused
              | ws_hasWindows ws          = colorUnfocused
              | otherwise                 = colorEmpty

        chars | ws_isUrgent ws         = ("!", "!")
              | ws_visibleOn ws == sid = ("[", "]")
              | ws_visibleOn ws > -1   = ("<", ">")
              | otherwise              = (" ", " ")


wrapColor :: Color -> String -> String
wrapColor color content = "<fc=" ++ color ++ ">" ++ content ++ "</fc>"

wrapText :: String -> String -> String -> String
wrapText l r t = l ++ t ++ r

separator :: String
separator = wrapColor colorPrimary " | "

main :: IO ()
main = xmobar
  =<< configWithDBusLog ppState
  =<< configFromArgs
  =<< getDefaultConfig

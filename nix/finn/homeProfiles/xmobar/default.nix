{ inputs, cell, pkgs }:

let inherit (inputs) barstate-hs;
in {
  programs.xmobar = {
    enable = true;
    # TODO: centralize theming
    package = pkgs.writers.writeHaskellBin "xmobar" {
      libraries = let inherit (pkgs) haskellPackages;
      in [
        haskellPackages.xmobar
        haskellPackages.unix
        barstate-hs.packages.default
      ];
    } (builtins.readFile ./_xmobar.hs);
  };

  xdg.configFile."xmobar/haskell_logo.xpm".source = ./_haskell_logo.xpm;
}

{ inputs, cell, config, lib }:

let
  inherit (cell) secrets;
  homeDir = config.home.homeDirectory;
in {
  programs.borgmatic = {
    enable = true;

    backups."home" = {
      location = {
        patterns = [
          "R ${homeDir}"

          # File types
          # Exclude .iso files
          "- **/*.iso"

          # General
          # System
          "- **/.cache"
          "- **/cache"
          "- **/Cache"
          "- ${homeDir}/.config"
          "- ${homeDir}/.local/state"
          "! ${homeDir}/.local/share/Trash"
          "- ${homeDir}/Downloads"
          # Nix
          "- **/.direnv"
          # OCI Containers
          "! ${homeDir}/.local/share/containers"

          # Applications
          # Arduino IDE
          "- ${homeDir}/.arduino15"
          # Firefox
          "- ${homeDir}/.mozilla/firefox"
          # JetBrains
          "- ${homeDir}/.local/share/JetBrains/Toolbox/apps"
          # Mathematica
          "- ${homeDir}/.Mathematica"
          "- ${homeDir}/.Wolfram"
          # perf
          "- ${homeDir}/.debug"
          # Steam
          "- ${homeDir}/.local/share/Steam"
          "+ ${homeDir}/.local/share/Steam/userdata"

          # Languages
          # Go
          "- ${homeDir}/.go/pkg"
          # Haskell
          "- ${homeDir}/.ghcup"
          "- ${homeDir}/.stack"
          "- **/.stack-work"
          "- ${homeDir}/.cabal"
          # TODO: cabal build files?
          # Java
          "- ${homeDir}/.java"
          "- ${homeDir}/.gradle"
          # JavaScript
          "- ${homeDir}/.npm"
          # LaTeX
          "- **/_minted-latex"
          # Rust
          "- ${homeDir}/.rustup"
          "- ${homeDir}/.cargo"
          "- **/target"
          # Python
          "- **/__pycache__"
        ];
        # sourceDirectories = [ homeDir ];

        extraConfig = {
          "compression" = "auto,zstd,3";

          "exclude_caches" = true;
          "exclude_nodump" = true;
        };
        excludeHomeManagerSymlinks = true;

        repositories = let
          useSecretRepos = repoNames:
            lib.pipe repoNames [
              # Lookup all repoNames first, so we get a warning if one is missing.
              (map (repoName: secrets [ "borgRepos" repoName ] null))
              (builtins.filter (repo: repo != null))
              (map (repo: {
                label = repo.label;
                path = "${repo.path}/{hostname}";
              }))
            ];
        in useSecretRepos [ "primary" "secondary" ];

        extraConfig."ssh_command" = "ssh -i ${homeDir}/.ssh/id_backup";
      };
    };
  };

  home.shellAliases = {
    "borgmatic-backup" = "borgmatic create -v 1 --progress --stats";
  };
}

{ inputs, cell, pkgs }:

{
  imports = [ cell.homeModules.theming ];

  # Need to be explicitly enabled for config generation.
  gtk.enable = true;
  qt.enable = true;

  home.pointerCursor.size = 16;

  theming = {
    theme = {
      primary = "gruvbox";
      mode = "dark";
      variant = "hard";
    };

    colors = {
      fg = "#ebdbb2";
      bg = "#1d2021";

      bg1 = "#3c3836";

      red = "#fb4934";
      blue = "#83a598";
    };

    font = {
      package = pkgs.iosevka-bin.override { variant = "SS06"; };
      name = "Iosevka Term SS06 Extended";
    };

    cursor = {
      package = pkgs.libsForQt5.breeze-qt5;
      name = "Breeze_Snow";
    };

    gtk = {
      theme = {
        package = pkgs.libsForQt5.breeze-gtk;
        name = "Breeze-Dark";
      };
      icons = {
        package = pkgs.libsForQt5.breeze-icons;
        name = "breeze-dark";
      };
    };

    qt = let package = pkgs.libsForQt5.breeze-qt5;
    in {
      theme = {
        inherit package;
        # TODO: QT theme is not in dark mode.
        name = "Breeze";
      };
    };
  };
}

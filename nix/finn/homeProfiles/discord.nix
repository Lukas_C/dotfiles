{ inputs, cell, lib, config, pkgs }:

{
  home.packages = with pkgs; [ discord ];

  home.activation = {
    # Activation script that disables host update checking:
    # https://nixos.wiki/wiki/Discord#.22Must_be_your_lucky_day.22_popup
    # TODO: Respect DRY_RUN, see documentation of `home.activation` option.
    discordDisableHostUpdate = let
      configDir = "${config.home.homeDirectory}/.config/discord";
      configFile = "${configDir}/settings.json";
    in lib.hm.dag.entryAfter [ "writeBoundary" ] ''
      if [[ ! -f "${configFile}" ]]; then
          mkdir -p "${configDir}"
          echo "{}" > "${configFile}"
      fi
      echo $(${pkgs.jq}/bin/jq '.SKIP_HOST_UPDATE = true' "${configFile}") > "${configFile}"
    '';
  };
}

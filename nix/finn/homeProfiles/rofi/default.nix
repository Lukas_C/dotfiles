{ inputs, cell, config, pkgs }:

{
  programs.rofi = {
    enable = true;
    plugins = with pkgs; [ rofi-calc ];

    extraConfig = {
      modi = "drun,calc";

      kb-element-next = ""; # unbind TAB
      kb-mode-next = "Tab";
      kb-mode-previous = "Shift+Tab";
    };

    # TODO: theming
    # See also:
    # https://github.com/davatorium/rofi/tree/next/themes
    # font = TODO
    theme = ./_theme.rasi;
  };
}

# Create a directory and cd inside it immediately.
# Source of the improved version:
# https://unix.stackexchange.com/a/125386
mkcd() { mkdir -p -- "$1" && cd -P -- "$1" }

# Mount device to standard /mnt location.
mnt() { sudo mount ${1:?'Please specify the device to mount'} /mnt && cd /mnt }
umnt() { cd ~ && sudo umount /mnt }
# Mount encrypted device to standard /mnt location.
cmnt() { sudo cryptsetup open ${1:?'Please specify the encrypted device to mount'} cryptusb && mnt /dev/mapper/cryptusb }
cumnt() { umnt && sudo cryptsetup close cryptusb }

# Display the colors of RGB hex tuples in the terminal.
show_color() {
    for color in "$@"; do
        print -P -n "%K{#$color} %k "
    done
    print
}

{ inputs, cell, config, lib }:

let
  cfg = config.programs.zsh;
  relToDotDir = file:
    (lib.optionalString (cfg.dotDir != null) (cfg.dotDir + "/")) + file;

  functionsFile = relToDotDir "functions.zsh";
in {
  programs.zsh = {
    enable = true;

    autocd = true;

    dotDir = ".config/zsh";

    initExtra = lib.concatStringsSep "\n" [
      # Disable bell/beep.
      "setopt nobeep"
      # Do not warn about running processes before exiting.
      "setopt nocheckjobs"

      # Case insensitive globbing.
      "setopt nocaseglob"
      # Sort filenames numerically when it makes sense.
      "setopt numericglobsort"

      # Immediately append history instead of overwriting
      "setopt appendhistory"
      # If a new command is a duplicate, remove the older one
      "setopt histignorealldups"

      # List completions automatically
      "setopt auto_list"

      # Extra Keybinds.
      # Home
      "bindkey '^[[H'    beginning-of-line"
      # End
      "bindkey '^[[F'    end-of-line"
      # Delete
      "bindkey '^[[3~'   delete-char"
      # Ctrl+Backspace
      "bindkey '^H'      backward-kill-word"
      # Esc-Esc
      "bindkey '^[^['    backward-kill-line"
      # Shift+Tab
      "bindkey '^[[Z'    undo"
      # Ctrl+Right
      "bindkey '^[[1;5C' forward-word"
      # Ctrl+Left
      "bindkey '^[[1;5D' backward-word"

      # Dedicated file for custom functions, etc.
      ''source "$HOME/${functionsFile}"''
    ];

    dirHashes = let xdg = config.xdg.userDirs;
    in {
      "doc" = xdg.documents;
      "dl" = xdg.download;
      "dev" = xdg.extraConfig.XDG_DEVELOPMENT_DIR;
      "work" = xdg.extraConfig.XDG_WORK_DIR;
    };

    enableCompletion = true;
    autosuggestion.enable = true;
    historySubstringSearch.enable = true;
    syntaxHighlighting.enable = true;
  };

  home.file."${functionsFile}".source = ./_functions.zsh;

  # NOTE: This is an activation script that would byte-compile all `*.zsh` files in `zdotdir`, ahead of time.
  # However, this would not process all files, most importantly `.zshrc`, also because this currently ignores symlinks.
  # At the moment, performance is not a problem, so this is kept just as a future reference.
  # home.activation.compileZshRc = let
  #   # Construct zdotdir analogously to zsh HM module, as we cannot access the path directly.
  #   zdotdir = "$HOME/" + cfg.dotDir;
  # in ''
  #   ${pkgs.findutils}/bin/find ${zdotdir} -maxdepth 1 -name '*.zsh' -exec ${pkgs.zsh}/bin/zsh -c 'zcompile "{}"' \;
  # '';
}

{ inputs, cell }:

{
  programs.firefox = {
    enable = true;

    policies = {
      # Extension management using firefox's built-in tools. See also:
      # https://discourse.nixos.org/t/declare-firefox-extensions-and-settings/36265
      # https://mozilla.github.io/policy-templates/#extensionsettings
      # TODO: https://wiki.webevaluation.nl/firefox_addons
      ExtensionSettings = let
        buildInstallUrl = extension_name:
          "https://addons.mozilla.org/firefox/downloads/latest/${extension_name}/latest.xpi";
        installation_mode = "normal_installed";
      in {
        # Default configuration.
        "*" = {
          allowed_types = [ "extension" "theme" "dictionary" ];

          # Block all other extensions.
          #installation_mode = "blocked";
          blocked_install_message =
            "Extensions are currently managed through the home-manager Nix config.";
        };

        # General extensions.
        # KeepassXC
        "keepassxc-browser@keepassxc.org" = {
          install_url = buildInstallUrl "keepassxc-browser";
          inherit installation_mode;
        };
        # uBlock Origin
        "uBlock0@raymondhill.net" = {
          install_url = buildInstallUrl "ublock-origin";
          inherit installation_mode;
        };
        # Privacy Badger
        "jid1-MnnxcxisBPnSXQ@jetpack" = {
          install_url = buildInstallUrl "privacy-badger17";
          inherit installation_mode;
        };

        # Website styling and customization extensions.
        # Darkreader
        "addon@darkreader.org" = {
          install_url = buildInstallUrl "darkreader";
          inherit installation_mode;
        };
        # Stylus
        "{7a7a4a92-a2a0-41d1-9fd7-1e92480d612d}" = {
          install_url = buildInstallUrl "styl-us";
          inherit installation_mode;
        };

        # Search extensions.
        ## Startpage
        #"{20fc2e06-e3e4-4b2b-812b-ab431220cada}" = {
        #  install_url = buildInstallUrl "startpage-private-search";
        #  inherit installation_mode;
        #};
        # Rust
        "{04188724-64d3-497b-a4fd-7caffe6eab29}" = {
          install_url = buildInstallUrl "rust-search-extension";
          inherit installation_mode;
        };

        # Themes.
        # gruvbox dark-hard by Saltbot
        "{06d0794d-1297-4343-b542-a408f483e940}" = {
          install_url = buildInstallUrl "gruvbox-dark-hard";
          inherit installation_mode;
        };

        # Dictionaries.
        # German dictionary by KaiRo
        "de-DE@dictionaries.addons.mozilla.org" = {
          install_url = buildInstallUrl "dictionary-german";
          inherit installation_mode;
        };
      };
    };

    profiles."default" = {
      isDefault = true;

      search = {
        force = true;

        default = "Startpage";
        order = [ "Startpage" "Wikipedia" ];
        privateDefault = "DuckDuckGo";

        engines = let
          # Default favicon update interval: once a day.
          updateInterval = 24 * 60 * 60 * 1000;

          mkNixPackagesEngine = { definedAliases, category }: {
            inherit definedAliases;
            urls = [{
              template = "https://search.nixos.org/${category}";
              iconUpdateURL = "https://nixos.org/favicon.png";
              inherit updateInterval;
              params = [
                {
                  name = "type";
                  value = "packages";
                }
                {
                  name = "query";
                  value = "{searchTerms}";
                }
                {
                  name = "channel";
                  value = "unstable";
                }
              ];
            }];
          };
        in {
          "Startpage" = {
            definedAliases = [ "g" ];

            urls = [{
              template = "https://www.startpage.com/sp/search";
              iconUpdateURL =
                "https://www.startpage.com/sp/cdn/favicons/favicon-gradient.ico";
              inherit updateInterval;
              params = [{
                name = "query";
                value = "{searchTerms}";
              }];
            }];
          };
          "Wikipedia (en)".metaData.alias = "w";
          "DuckDuckGo".metaData.alias = "ddg";

          "Youtube" = {
            definedAliases = [ "y" ];

            urls = [{
              template = "https://www.youtube.com/results";
              iconUpdateURL = "https://www.youtube.com/img/favicon.ico";
              inherit updateInterval;
              params = [{
                name = "search_query";
                value = "{searchTerms}";
              }];
            }];
          };

          "Nix Packages" = mkNixPackagesEngine {
            definedAliases = [ "np" ];
            category = "packages";
          };
          "NixOS Options" = mkNixPackagesEngine {
            definedAliases = [ "no" ];
            category = "options";
          };

          "Amazon.de".metaData.hidden = true;
          "Google".metaData.hidden = true;
          "Bing".metaData.hidden = true;
        };
      };

      # NOTE: Extensions are currently managed indirectly by utilizing firefox's "ExtensionSettings" policy.
      extensions = [ ];

      settings = {
        # Disable `about:config` warning.
        "browser.aboutConfig.showWarning" = false;
        # Allow `*.lan` as valid tld and prevent search requests.
        "browser.fixup.domainsuffixwhitelist.lan" = true;
        # Restore previous session on startup.
        "browser.startup.page" = 3;
        # Keep window open when last tab is closed.
        "browser.tabs.closeWindowWithLastTab" = false;
        # Devtools dark theme.
        "devtools.theme" = "dark";
        # Show all search occurences on the scrollbar.
        "findbar.highlightAll" = true;
        # Use (correct) system locales, fixing incorrect date pickers, etc.
        "intl.regional_prefs.use_os_locales" = true;
        # Disable left and right swipe to go to previous and next webpage, respectively.
        "browser.gesture.swipe.left" = "";
        "browser.gesture.swipe.right" = "";

        # Disable autofill for sensitive information.
        "signon.rememberSignons" = false;
        "extensions.formautofill.creditCards.enabled" = false;

        # The following settings are courtesy of:
        # https://wiki.webevaluation.nl/firefox_privacy_and_security

        # Disable advertising.
        "browser.startup.homepage" = "about:home";
        "browser.newtab.url" = "about:home";
        "layers.enable-tiles" = false;
        "layers.tiles.adjust" = false;

        # Disable geolocation.
        "geo.enabled" = false;
        "geo.wifi.logging.enabled" = false;
        # 0100::/64 is the IPv6 blackhole prefix
        "geo.wifi.uri" = "[0100::]";
        "geo.provider.network.url" = "[0100::]";

        # Disable telemetry.
        "datareporting.healthreport.service.enabled" = false;
        "datareporting.healthreport.uploadEnabled" = false;
        "browser.newtabpage.activity-stream.feeds.telemetry" = false;
        "browser.newtabpage.activity-stream.telemetry" = false;
        "browser.ping-centre.telemetry" = false;
        "toolkit.telemetry.enabled" = false;
        "toolkit.telemetry.archive.enabled" = false;
        "toolkit.telemetry.bhrPing.enabled" = false;
        "toolkit.telemetry.firstShutdownPing.enabled" = false;
        "toolkit.telemetry.hybridContent.enabled" = false;
        "toolkit.telemetry.newProfilePing.enabled" = false;
        "toolkit.telemetry.reportingpolicy.firstRun" = false;
        "toolkit.telemetry.server" = "";
        "toolkit.telemetry.shutdownPingSender.enabled" = false;
        "toolkit.telemetry.shutdownPingSender.enabledFirstSession" = false;
        "toolkit.telemetry.unified" = false;
        "toolkit.telemetry.updatePing.enabled" = false;
        "beacon.enabled" = false;
        "privacy.firstparty.isolate" = true;

        # Disable Loop and Pocket.
        "loop.enabled" = false;
        "browser.pocket.enabled" = false;
        "extensions.pocket.enabled" = false;

        # Additional privacy options.
        "privacy.globalprivacycontrol.enabled" = true;
        "privacy.globalprivacycontrol.functionality.enabled" = true;
      };
    };
  };

  xdg.mimeApps = {
    associations.added = {
      "application/pdf" = "firefox.desktop";
      "image/svg+xml" = "firefox.desktop";
    };
  };
}

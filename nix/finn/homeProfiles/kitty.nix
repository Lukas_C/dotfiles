{ inputs, cell, lib, pkgs, config }:

let kittyPackage = pkgs.kitty;
in {
  programs.kitty = {
    enable = true;
    package = kittyPackage;

    keybindings = { "CTRL+n" = "new_os_window_with_cwd"; };
    settings = {
      # Don't "drrrring" me!
      enable_audio_bell = false;
      # Classic, "block-style" cursor.
      shell_integration = "no-cursor";
      # Disable confirmation dialog when a command is still running.
      confirm_os_window_close = 0;
    };

    font = let inherit (config.theming) font;
    in {
      package = font.package;
      name = font.name;
      size = 11;
    };

    themeFile = with config.theming.theme; "${primary}-${mode}-${variant}";
  };

  # Export as default terminal
  home.sessionVariables = {
    TERMINAL = lib.mkDefault "${kittyPackage}/bin/kitty";
  };
  systemd.user.sessionVariables = {
    TERMINAL = lib.mkDefault "${kittyPackage}/bin/kitty";
  };
}

{ input, cell, config }:

let homeDir = config.home.homeDirectory;
in {
  xdg = {
    enable = true;

    userDirs = {
      enable = true;
      createDirectories = true;

      # disable some unused dirs
      desktop = null;
      publicShare = null;

      extraConfig = {
        XDG_DEVELOPMENT_DIR = "${homeDir}/Development";
        XDG_WORK_DIR = "${homeDir}/Work";
        # TODO: decide wether to use
        #XDG_TEMP_DIR = "${homeDir}/Temp";
      };
    };
  };

  # If XDG support is enabled, fix some default locations.
  # Configuration here should not lead to the creation of these files.
  xresources.path = "${config.xdg.configHome}/X11/xresources";
}

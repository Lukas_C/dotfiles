;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

(setq user-full-name "Lukas_C"
      user-mail-address nil)

(load-theme 'gruvbox-dark-hard t)

;; Enable line numbers.
(setq display-line-numbers-type t)

;; Directory for org files.
(setq org-directory "$XDG_DOCUMENTS_DIR/org/")
;; Do not collapse links.
;; Toggle at runtime with =org-toggle-link-display=.
(setq org-link-descriptive nil)

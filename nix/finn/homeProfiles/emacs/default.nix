{ inputs, cell, lib, config, pkgs }:

let
  emacsPackage = pkgs.emacs;
  # NOTE: pgtk is not supported on X11.
  #emacsPackage = pkgs.emacs29-pgtk;
  emacsDir = "${config.xdg.configHome}/emacs";
in {
  # Emacs daemon
  services.emacs = {
    enable = true;
    package = emacsPackage;
    defaultEditor = true;
    startWithUserSession = "graphical";
  };

  # Config
  xdg.configFile."doom".source = ./_doom;

  fonts.fontconfig.enable = true;
  home.sessionPath = [ "${emacsDir}/bin" ];

  # Doom emacs dependencies, see:
  # https://github.com/hlissner/dotfiles/blob/089f1a9da9018df9e5fc200c2d7bef70f4546026/modules/editors/emacs.nix
  home.packages = with pkgs; [
    # Core
    emacsPackage
    emacs-all-the-icons-fonts
    binutils # Required for native-comp
    git
    ripgrep
    gnutls

    # Additional deps
    fd
    zstd

    # :checkers
    # spell
    (aspellWithDicts (ds:
      # For a complete list of available dictionaries, see:
      # https://github.com/NixOS/nixpkgs/blob/master/pkgs/development/libraries/aspell/dictionaries.nix
      with ds; [
        en
        en-computers
        en-science

        de
      ]))
    # grammar
    languagetool

    # :tools
    nodejs # npm is required for parts of lsp-mode
    editorconfig-core-c
    sqlite # lookup
    tree-sitter

    # :lang
    # data
    libxml2 # xmllint
    # json/js/ts
    nodePackages.prettier
    html-tidy
    stylelint
    jsbeautifier
    # nix
    # TODO: migrate to RFC-166 nixfmt (nixfmt-rfc-style)
    nixfmt-classic
    # python
    black
    # sh
    shfmt
    shellcheck
  ];

  # Automatic installation.
  # Code snipped adapted from:
  # https://github.com/hlissner/dotfiles/blob/089f1a9da9018df9e5fc200c2d7bef70f4546026/modules/editors/emacs.nix
  home.activation = let doomUrl = "https://github.com/doomemacs/doomemacs";
  in {
    installDoomEmacs = lib.hm.dag.entryAfter [ "writeBoundary" ] ''
      if [ ! -d "${emacsDir}" ]; then
         ${pkgs.git}/bin/git clone --depth=1 --single-branch "${doomUrl}" "${emacsDir}"
      fi
    '';
  };

  xdg.mimeApps = let
    defaults = {
      # Empty files
      "application/x-zerosize" = "emacsclient.desktop";

      # :lang files
      "application/json" = "emacsclient.desktop";
      "application/x-python3" = "emacsclient.desktop";

      # Custom mime type for `org-protocol`:
      # https://orgmode.org/worg/org-contrib/org-protocol.html
      "x-scheme-handler/org-protocol" = "emacsclient.desktop";
    };
  in {
    associations.added = defaults;
    defaultApplications = defaults;
  };
}

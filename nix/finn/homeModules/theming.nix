{ inputs, cell, lib, config }:

let
  cfg = config.theming;

  inherit (lib) types mkOption;
in {
  options = {
    theming = {
      theme = {
        primary = mkOption { type = with types; str; };
        mode = mkOption { type = with types; enum [ "dark" "light" ]; };
        variant = mkOption { type = with types; str; };
      };

      # TODO: add color regex?
      colors = {
        fg = mkOption { type = with types; str; };
        bg = mkOption { type = with types; str; };

        bg1 = mkOption { type = with types; str; };

        red = mkOption { type = with types; str; };
        blue = mkOption { type = with types; str; };
      };

      font = {
        package = mkOption { type = with types; package; };
        name = mkOption { type = with types; str; };
      };

      cursor = {
        package = mkOption { type = with types; package; };
        name = mkOption { type = with types; str; };
      };

      gtk = {
        theme = {
          package = mkOption { type = with types; package; };
          name = mkOption { type = with types; str; };
        };
        icons = {
          package = mkOption { type = with types; package; };
          name = mkOption { type = with types; str; };
        };
      };

      qt = {
        theme = {
          package = mkOption { type = with types; package; };
          name = mkOption { type = with types; str; };
        };
      };
    };
  };

  config = {
    home.packages = [ cfg.font.package ];

    # NOTE: Without using an .xsession and using a DE/WM that does not call
    # `xsetroot -cursor_name left_ptr` or sets the cursor otherwise,
    # the cursor style will not be applied on the root window.
    home.pointerCursor = {
      gtk.enable = true;
      x11.enable = true;
      inherit (cfg.cursor) package name;
    };

    gtk.theme = { inherit (cfg.gtk.theme) package name; };
    gtk.iconTheme = { inherit (cfg.gtk.icons) package name; };

    qt.style = { inherit (cfg.qt.theme) package name; };
  };
}

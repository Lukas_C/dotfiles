{ inputs, cell }:

let
  inherit (inputs) std agenix-rekey disko;
  inherit (cell) pkgs installables;
in {
  default = std.lib.dev.mkShell {
    commands = [
      { package = std.packages.default; }
      {
        name = "nh";
        package = installables.nh-secrets;
      }
      {
        package = disko.packages.default;
        category = "installation";
      }
      {
        package = installables.agenix-rekey-secrets;
        category = "secrets";
      }
    ];

    env = [
      {
        name = "FLAKE_DOTFILES";
        eval = "$PRJ_ROOT";
      }
      # Set primary identity from available Yubikeys.
      # See also:
      # https://github.com/oddlama/agenix-rekey/pull/28
      {
        name = "AGENIX_REKEY_PRIMARY_IDENTITY";
        eval = with pkgs;
          "$(${age-plugin-yubikey}/bin/age-plugin-yubikey --list | ${gnugrep}/bin/grep '^age1yubikey1' | ${coreutils}/bin/head -n 1)";
      }
    ];
  };
}

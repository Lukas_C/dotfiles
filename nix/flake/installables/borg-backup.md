A wrapper for `borg` (<https://www.borgbackup.org>)
which simplifies the backup creation process.
It aims to create an unique repository name each time the script is called,
preventing naming conflicts while maintaining searchability.

# Environment Variables and Defaults
Below are the available environment variables and their default values.

```sh
# Remote backup server.
# Customization of the connection can be achieved through a SSH config.
BACKUP_SERVER="ssh://backup/./"
# Repo name.
BACKUP_REPO="main"
# Hostname of the device where the backup is from.
BACKUP_HOSTNAME="$(cat /etc/hostname)"
# OS/Distro of the backup.
BACKUP_OS="$(. /etc/os-release; echo $NAME)"
# Folder to read excludes from. See below for specifics.
EXCLUDE_FOLDER="$XDG_CONFIG_HOME/borg-backup"
```

# Excludes
The script provides a mechanism to exclude specific directories,
based on the `--exclude-from` flag of `borg`.
It takes a file that contains a list of paths that should be ignored.
This implementation supplies all filed contained in a folder that is specified
by the `EXCLUDE_FOLDER` variable to borg.
The files are piped through `envsubst` beforehand,
allowing for substitution of environment variables, such as `$HOME`.

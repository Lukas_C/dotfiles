A custom version of the [`agenix-rekey` CLI](https://github.com/oddlama/agenix-rekey/blob/main/nix/package.nix)
that can automatically deal with an additional flake for secret management.
By default, the wrapped version will look in the following places, in order:

- If a `FLAKE_SECRETS` environment variable is present, it will use that.
- Next, it will check if `$FLAKE_DOTFILES/secrets` exists.
- Next, it will check if `$FLAKE_DOTFILES/dotfiles-secrets` exists.

The selected path will then be passed as extra arguments to the underlying `nix run` command, like so:

```sh
nix run --override-input "secrets" <FLAKE_SECRETS> <extra args after "--"> .#agenix-rekey.<system>.<app> <args>
```

This also allows for additional arguments like `--show-trace` to be passed to `nix run`.

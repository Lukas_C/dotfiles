args=()
extraArgs=()

# Select which secrets directory to use.
secrets_dir=""
FLAKE_SECRETS=${FLAKE_SECRETS:-} # Explicity set to the empty string if unset.
if [[ -n "$FLAKE_SECRETS" ]]; then
    secrets_dir="$FLAKE_SECRETS"
elif [[ -d "$FLAKE_DOTFILES/secrets" ]]; then
    secrets_dir="$FLAKE_DOTFILES/secrets"
elif [[ -d "$FLAKE_DOTFILES/dotfiles-secrets" ]]; then
    secrets_dir="$FLAKE_DOTFILES/dotfiles-secrets"
fi

# Split arguments at "--" and pass argument groups separately.
while [[ $# -gt 0 ]]; do
    if [[ "$1" == "--" ]]; then
        shift
        extraArgs=("$@")
        break
    else
        args+=("$1")
        shift
    fi
done

if [[ -z "$secrets_dir" ]]; then
    echo "could not locate a secrets flake, continuing without..."
else
    extraArgs+=("--override-input" "secrets" "$secrets_dir")
fi

# Pass extra args to `nix run` and everything else to `agenix-rekey`.
# The `$system` variable is passed from the outside by `writeShellApplication`.
exec nix run "${extraArgs[@]}" .#agenix-rekey."$system"."${args[0]}" "--" "${args[@]:1}"

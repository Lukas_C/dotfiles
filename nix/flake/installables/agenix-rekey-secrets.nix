{ writeShellApplication, lib, stdenv }:
writeShellApplication {
  name = "agenix";
  meta = {
    description = "Manage and rekey secrets, even with secret flakes";

    longDescription = lib.readFile ./agenix-rekey-secrets.md;
  };

  runtimeEnv = { inherit (stdenv.hostPlatform) system; };

  text = lib.readFile ./agenix-rekey-secrets.sh;
}

{ inputs, cell }:

let inherit (cell.pkgs) callPackage;
in {
  agenix-rekey-secrets = callPackage ./agenix-rekey-secrets.nix { };
  borg-backup = callPackage ./borg-backup.nix { };
  nh-secrets = callPackage ./nh-secrets.nix { };
}

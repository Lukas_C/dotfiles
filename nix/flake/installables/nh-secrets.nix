{ writeShellApplication, lib, nh }:
writeShellApplication {
  name = "nh";
  meta = {
    description = "${nh.meta.description}, but with secret flakes";

    longDescription = lib.readFile ./nh-secrets.md;
  };

  runtimeInputs = [ nh ];

  text = lib.readFile ./nh-secrets.sh;
}

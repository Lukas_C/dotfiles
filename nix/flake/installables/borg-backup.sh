cd "$HOME"

# Remote.
BACKUP_SERVER=${BACKUP_SERVER:-"ssh://backup/./"}
BACKUP_REPO=${BACKUP_REPO:-"main"}

# Full repo path.
_REPO="$BACKUP_SERVER$BACKUP_REPO"
echo "Remote: $_REPO"

echo ""
echo "Backup:"

BACKUP_HOSTNAME=${BACKUP_HOSTNAME:-"$(cat /etc/hostname)"}
echo "  Hostname: $BACKUP_HOSTNAME"
BACKUP_OS=${BACKUP_OS:-"$(
  # Spec guaratees the file to be valid bash variable assignments:
  # https://www.freedesktop.org/software/systemd/man/latest/os-release.html
  # shellcheck disable=SC1091
  . /etc/os-release
  echo "$NAME"
)"}
echo "  OS: $BACKUP_OS"
_USER="$(whoami)"
echo "  User: $_USER"
# This script creates backups of the user data located at $HOME.
_TYPE="home"

_TIMESTAMP_FULL="$(TZ='UTC' date --iso-8601=seconds)"
# Reusing the acquired UTC timestamp ensures consistency.
_LOCALTIME="$(date -d "$_TIMESTAMP_FULL" +'%H:%M %d.%m.%Y (%Z)')"
# Strip redundant time zone offset.
# shellcheck disable=SC2001 # End of line matching cannot be perfomed by parameter expansion.
_TIMESTAMP="$(echo "$_TIMESTAMP_FULL" | sed 's/+00:00$//g')"
echo "  Time: $_LOCALTIME (UTC: $_TIMESTAMP)"

echo ""

# Full archive name. Replace whitespace with dashes for safety.
_ARCHIVE="$(echo "$BACKUP_HOSTNAME.$BACKUP_OS.$_USER.$_TYPE.$_TIMESTAMP" | sed 's/[[:space:]]/-/g')"
echo "Full archive name: $_ARCHIVE"

echo ""

DEFAULT_EXCLUDE_FOLDER="${XDG_CONFIG_HOME:-"$HOME/.config"}/borg-backup/"
EXCLUDE_FOLDER="${EXCLUDE_FOLDER:-"$DEFAULT_EXCLUDE_FOLDER"}"

borg \
  --show-version \
  --show-rc \
  --progress \
  create \
  --stats \
  --list \
  --filter E \
  --exclude-from <(
    # Use all files present in the exclude_folder
    if [ -d "$EXCLUDE_FOLDER" ] && [ -n "$(ls -A "$EXCLUDE_FOLDER")" ]; then
      cat "$EXCLUDE_FOLDER"/*
    fi | envsubst
  ) \
  --comment "Backup at $_LOCALTIME" \
  --compression zstd \
  "$_REPO::$_ARCHIVE" \
  "$HOME"

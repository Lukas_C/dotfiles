A wrapped version of `nh` (<https://github.com/viperML/nh>)
that can automatically deal with an additional flake for secret management.
By default, the wrapped version will look in the following places, in order:

- If a `FLAKE_SECRETS` environment variable is present, it will use that.
- Next, it will check if `$FLAKE_DOTFILES/secrets` exists.
- Next, it will check if `$FLAKE_DOTFILES/dotfiles-secrets` exists.

The selected path will then be passed as extra arguments to `nh os` or `nh home` like so:

```sh
nh <args> -- --override-input "secrets" <FLAKE_SECRETS> <extra args after "--">
```

Note that the functionality that allows one to pass `EXTRA_ARGS` to the underlying commands using the "--" syntax is maintained.
As such, this wrapped version should act completely transparently and all command resources about `nh` remain valid.


{ writeShellApplication, lib, coreutils, gnused, envsubst, borgbackup }:
writeShellApplication {
  name = "borg-backup";
  meta = {
    description =
      "borg wrapper for simplified archive creation with robust naming";

    longDescription = lib.readFile ./borg-backup.md;
  };

  runtimeInputs = [
    coreutils
    gnused
    envsubst

    borgbackup
  ];

  text = lib.readFile ./borg-backup.sh;
}

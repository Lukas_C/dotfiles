{ inputs, cell }:

let inherit (inputs) haumea;
in haumea.lib.load {
  src = ./.;
  inputs = { inherit inputs cell; };
}
